#!/bin/bash

DATA=$(date +%y-%m-%d-%H)
mkdir /tmp/$DATA
cp ./* /tmp/$DATA 2> /dev/null

tar -czf data_compac.tar.gz /tmp/$DATA 2> /dev/null
cp /tmp/$DATA/data_compac.tar.gz ~
rm -rf /tmp/$DATA
