#!/bin/bash

echo -e "\e[1;34mSubstituição de Variáveis \e[0m"
echo "Procura todos os sinais '$' antes de executar o comando e substitui-os pelo valor da variável"
echo "Ex.: \$1, \$variavel"
read

echo -e "\e[1;34mSubstituição de Shell \e[0m"
echo "Usando parênteses, guarda um comando"
echo "Ex.: \$(date +%y-%m-%d)"
read

echo -e "\e[1;34mSubstituição de Aritmética \e[0m"
echo "Substitui pelo valor da expressão aritmética entre parênteses duplos"
echo "Ex.: \$((valor))"
